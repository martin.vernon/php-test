<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePokemonSpritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokemon_sprites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('pokemon_id');
            $table->string('front_default')->nullable();	
            $table->string('front_shiny')->nullable();	
            $table->string('front_female')->nullable();
            $table->string('front_shiny_female')->nullable();
            $table->string('back_default')->nullable();	
            $table->string('back_shiny')->nullable();	
            $table->string('back_female')->nullable();	        
            $table->string('back_shiny_female')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pokemon_sprites');
    }
}
