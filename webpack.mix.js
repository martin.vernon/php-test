const mix = require('laravel-mix');
const CleanWebpackPlugin = require('clean-webpack-plugin');

// paths to clean
var pathsToClean = [
    'public/js',
    'public/css',
    'public/fonts',
    'public/images',
    'public/img'
];

// the clean options to use
var cleanOptions = {};

mix.webpackConfig({
    plugins: [
        new CleanWebpackPlugin(pathsToClean, cleanOptions)
    ],
    resolve: {
        symlinks: false,
        alias: {
            'masonry': 'masonry-layout',
            'isotope': 'isotope-layout'
        }
    }
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 |--------------------------------------------------------------------------
 | Core
 |--------------------------------------------------------------------------
 */
mix
    .options({
        processCssUrls: true,
        imgLoaderOptions: {
            enabled: false
        }
    });

mix
    .sass('resources/sass/app.scss', 'public/css').version()
    .js('resources/js/app.js', 'public/js').version()
    .autoload({
        switchery: ['Switchery'],
        nprogress: ['NProgress'],
        tinymce: ['tinymce'],
        daterangepicker: ['daterangepicker'],
        moment: ['moment'],
        dropzone: ['Dropzone'],
        imagesloaded: ['imagesLoaded'],
        'masonry-layout': ['Masonry'],
        'isotope-layout': ['Isotope'],
        velocity: ['velocity-animate'],
        fullcalendar: ['fullcalendar'],
        contextmenu: ['contextMenu']
    })
    .copy('node_modules/tinymce/skins/lightgray', 'public/css/tinymce/lightgray', false);