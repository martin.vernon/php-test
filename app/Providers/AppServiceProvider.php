<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Form::macro('searchfrm', function( $route, $class = 'col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search', $hidden = null ){
            $html = $this->open(['route'=>$route, 'method'=>'GET', 'class'=>$class] ) .
                '<div class="input-group">'.
                    $this->input('text','s', null, ['class'=>'form-control', 'placeholder'=>'Search for...']);
                    
                    if($hidden != null){
                        foreach($hidden as $k => $v){
                            $html .= $this->hidden($k, $v);
                        }
                    }
                    
                    $html .= '<span class="input-group-btn">'.
                        $this->button('Go', ['type'=>'submit', 'class'=>'btn btn-default btn-go animateMe']) .
                        '<a href="'. url()->current(). '"><button type="button" class="btn btn-danger btn-clear animateMe">Clear</button></a>' .
                    '</span>'.
                '</div>'.
                $this->close();
            return $html;
        });
    }
}
