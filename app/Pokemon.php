<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
    protected $fillable = [
        'name',
        'base_experience',
        'height',
        'is_default',
        'order',
        'weight',
        'location_area_encounters',
        'species'
    ];
    
    public function sprites(){
        return $this->hasOne('App\PokemonSprite');
    }

    public function abilities(){
        return $this->hasMany('App\PokemonAbility')->orderby('slot', 'ASC');
    }
}
