<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PokemonAbility extends Model
{
    protected $fillable = [
        'pokemon_id',
        'name',
        'slot',
        'is_hidden'
    ];

    public function pokemon(){
        return $this->belongsTo('App\Pokemon');
    }
}
