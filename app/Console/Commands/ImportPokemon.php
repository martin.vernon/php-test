<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PokePHP\PokeApi;
use App\Pokemon;
use App\PokemonSprite;
use App\PokemonAbility;

class ImportPokemon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:pokemon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports Pokemon into the database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Importing Pokemon into the database.');
        $api = new PokeApi;
        $all_pokemon = json_decode($api->resourceList('pokemon'));
        $this->info('Found '.$all_pokemon->count.' Pokemon to import');
        $bar = $this->output->createProgressBar($all_pokemon->count);
        $bar->start();
        for($i = 1; $i <= $all_pokemon->count; $i++){
            $pokemon_data = json_decode($api->pokemon($i));
            $pokemon = Pokemon::create([
                'name' => $pokemon_data->name,
                'base_experience' => $pokemon_data->base_experience,
                'height' => $pokemon_data->height,
                'is_default' => $pokemon_data->is_default,
                'order' => $pokemon_data->order,
                'weight' => $pokemon_data->weight,
                'location_area_encounters' => $pokemon_data->location_area_encounters,
                'species' => $pokemon_data->species->name
            ]);
            $sprites = PokemonSprite::create([
                'pokemon_id' => $pokemon->id,
                'front_default' => $pokemon_data->sprites->front_default,	
                'front_shiny' => $pokemon_data->sprites->front_shiny,	
                'front_female' => $pokemon_data->sprites->front_female,	
                'front_shiny_female' => $pokemon_data->sprites->front_shiny_female,
                'back_default' => $pokemon_data->sprites->back_default,	
                'back_shiny' => $pokemon_data->sprites->back_shiny,	
                'back_female' => $pokemon_data->sprites->back_female,	        
                'back_shiny_female' => $pokemon_data->sprites->back_shiny_female,
            ]);
            foreach($pokemon_data->abilities as $ability){
                PokemonAbility::create([
                    'pokemon_id' => $pokemon->id,
                    'name' => $ability->ability->name,
                    'slot' => $ability->slot,
                    'is_hidden' => $ability->is_hidden,
                ]);
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('Import complete');
    }
}
