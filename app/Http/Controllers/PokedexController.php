<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pokemon;
use Illuminate\Support\Collection;
use CollectionPaginator;

class PokedexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pokemon = Pokemon::where(function($query) use ($request){
            if(!empty($request->s))
                $query->where('name', 'like', $request->s);
        })->get();

        $pokemon = (isset($pokemon) && !empty($pokemon) && $pokemon->count() > 0) ? CollectionPaginator::paginate($pokemon, 20) : CollectionPaginator::paginate(new Collection(), 20);
        return view('index', [
            'pokemon' => $pokemon,
            's' => $request->get('s')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pokemon $pokemon)
    {
        return view('show', [
            'pokemon' => $pokemon
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
