<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PokemonSprite extends Model
{
    protected $fillable = [
        'pokemon_id',
        'front_default',	
        'front_shiny',	
        'front_female',	
        'front_shiny_female',
        'back_default',	
        'back_shiny',	
        'back_female',	        
        'back_shiny_female',
    ];

    public function pokemon(){
        return $this->belongsTo('App\Pokemon');
    }
}
