
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// gentelella node modules requirements
require('parsleyjs');
require('select2');
window.daterangepicker = require('daterangepicker');
window.moment = require('moment');
require('icheck');
window.Dropzone = require('dropzone');
window.Switchery = require('switchery');
require('image-picker');
require('imagesloaded');
require('masonry-layout');
require('jquery.scrollbar');
require('fullcalendar');
require('fullcalendar-scheduler');
window.contextMenu = require('jquery-contextmenu');
require('jquery.ui.position');
require('@fortawesome/fontawesome-free/js/all.js');
require('@fortawesome/fontawesome-free/js/v4-shims.js');

require('./tinymce');

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CURRENT_URL = window.location.href.split('?')[0],
    $BODY = $('body'),
    $MENU_TOGGLE = $('#menu_toggle'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $FOOTER = $('footer');

// Sidebar
$(document).ready(function() {
    // TODO: This is some kind of easy fix, maybe we can improve this
    var setContentHeight = function () {
        // reset height
        $RIGHT_COL.css('min-height', $(window).height());

        var bodyHeight = $BODY.outerHeight(),
            footerHeight = $BODY.hasClass('footer_fixed') ? 0 : $FOOTER.height(),
            leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
            contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

        // normalize content
        contentHeight -= $NAV_MENU.height() + footerHeight;

        $RIGHT_COL.css('min-height', contentHeight);
    };

    $SIDEBAR_MENU.find('a').on('click', function(ev) {
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function() {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                $SIDEBAR_MENU.find('li ul').slideUp();
            }

            $li.addClass('active');

            $('ul:first', $li).slideDown(function() {
                setContentHeight();
            });
        }
    });

    // toggle small or large menu
    $MENU_TOGGLE.on('click', function() {
        if ($BODY.hasClass('nav-md')) {
            $SIDEBAR_MENU.find('li.active ul').hide();
            $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
        } else {
            $SIDEBAR_MENU.find('li.active-sm ul').show();
            $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
        }

        $BODY.toggleClass('nav-md nav-sm');

        setContentHeight();
    });

    // check active menu
    $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

    $SIDEBAR_MENU.find('a').filter(function () {
        return this.href == CURRENT_URL;
    }).parent('li').addClass('current-page').parents('ul').filter(function () {
        if ($BODY.hasClass('nav-md')) {
            this.slideDown(function () {
                setContentHeight();
            });
        }
    }).parent().addClass('active');

    // recompute content when resizing
    $(window).smartresize(function(){
        setContentHeight();
    });

    setContentHeight();

    // fixed sidebar
    if ($.fn.mCustomScrollbar) {
        $('.menu_fixed').mCustomScrollbar({
            autoHideScrollbar: true,
            theme: 'minimal',
            mouseWheel:{ preventDefault: true }
        });
    }
});
// /Sidebar

// Panel toolbox
$(document).ready(function() {
    $('.collapse-link').on('click', function() {
        var $BOX_PANEL = $(this).closest('.x_panel'),
            $ICON = $(this).find('i'),
            $BOX_CONTENT = $BOX_PANEL.find('.x_content');

        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function(){
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200);
            $BOX_PANEL.css('height', 'auto');
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    });

    $('.close-link').click(function () {
        var $BOX_PANEL = $(this).closest('.x_panel');

        $BOX_PANEL.remove();
    });
});
// /Panel toolbox

// Tooltip
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

    $('.inputWrap').click(function(){
        var $arrowPoint = $(this).find('.pointTrig');
        var $selectBox = $(this).find('select2-container');
        if ( $selectBox.hasClass('select2-container--focus') ){
            $arrowPoint.toggleClass('fa-angle-down fa-angle-up');
        } else if ($selectBox.hasClass('select2-container--focus')){
            $arrowPoint.toggleClass('fa-angle-up fa-angle-down');
        } else{
        }
    });

    $('.menuTrig').click(function(){
       $('.menuWrapMob').slideToggle();
    });

});
// /Tooltip

// Progressbar
if ($(".progress .progress-bar")[0]) {
    $('.progress .progress-bar').progressbar();
}
// /Progressbar

// jquery.scrollbar
$(document).ready(function() {
    $('.scrollbar-dynamic').scrollbar();
});
// jquery.scrollbar

// Switchery
$(document).ready(function() {
    $('head style:last').remove();
    if ($(".js-switch")[0]) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#26B99A'
            });
        });
    }
});
// /Switchery

// iCheck
$(document).ready(function() {
    if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    }
});
// /iCheck

// Table
$('table input').on('ifChecked', function () {
    checkState = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('table input').on('ifUnchecked', function () {
    checkState = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});

var checkState = '';

$('.bulk_action input').on('ifChecked', function () {
    checkState = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('.bulk_action input').on('ifUnchecked', function () {
    checkState = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});
$('.bulk_action input#check-all').on('ifChecked', function () {
    checkState = 'all';
    countChecked();
});
$('.bulk_action input#check-all').on('ifUnchecked', function () {
    checkState = 'none';
    countChecked();
});

function countChecked() {
    if (checkState === 'all') {
        $(".bulk_action input[name='table_records']").iCheck('check');
    }
    if (checkState === 'none') {
        $(".bulk_action input[name='table_records']").iCheck('uncheck');
    }

    var checkCount = $(".bulk_action input[name='table_records']:checked").length;

    if (checkCount) {
        $('.column-title').hide();
        $('.bulk-actions').show();
        $('.action-cnt').html(checkCount + ' Records Selected');
    } else {
        $('.column-title').show();
        $('.bulk-actions').hide();
    }
}

// Accordion
$(document).ready(function() {
    $(".expand").on("click", function () {
        $(this).next().slideToggle(200);
        $expand = $(this).find(">:first-child");

        if ($expand.text() == "+") {
            $expand.text("-");
        } else {
            $expand.text("+");
        }
    });
});

// Select 2
$(document).ready(function() {
    $('.select2').select2();
    $('.select2-multiple').select2({
        multiple : true
    });
});

// NProgress
if (typeof NProgress != 'undefined') {
    NProgress.start();
    var interval = setInterval(function() { NProgress.inc(); }, 1000);
    $(document).ready(function () {
        clearInterval(interval);
        NProgress.done();
    });
}
//Clock
/*!
 * ClockPicker v0.0.7 (http://weareoutman.github.io/clockpicker/)
 * Copyright 2014 Wang Shenwei.
 * Licensed under MIT (https://github.com/weareoutman/clockpicker/blob/gh-pages/LICENSE)
 */
! function() {
    function t(t) {
        return document.createElementNS(p, t)
    }

    function i(t) {
        return (10 > t ? "0" : "") + t
    }

    function e(t) {
        var i = ++m + "";
        return t ? t + i : i
    }

    function s(s, r) {
        function p(t, i) {
            var e = u.offset(),
                s = /^touch/.test(t.type),
                o = e.left + b,
                n = e.top + b,
                p = (s ? t.originalEvent.touches[0] : t).pageX - o,
                h = (s ? t.originalEvent.touches[0] : t).pageY - n,
                k = Math.sqrt(p * p + h * h),
                v = !1;
            if (!i || !(g - y > k || k > g + y)) {
                t.preventDefault();
                var m = setTimeout(function() {
                    c.addClass("clockpicker-moving")
                }, 200);
                l && u.append(x.canvas), x.setHand(p, h, !i, !0), a.off(d).on(d, function(t) {
                    t.preventDefault();
                    var i = /^touch/.test(t.type),
                        e = (i ? t.originalEvent.touches[0] : t).pageX - o,
                        s = (i ? t.originalEvent.touches[0] : t).pageY - n;
                    (v || e !== p || s !== h) && (v = !0, x.setHand(e, s, !1, !0))
                }), a.off(f).on(f, function(t) {
                    a.off(f), t.preventDefault();
                    var e = /^touch/.test(t.type),
                        s = (e ? t.originalEvent.changedTouches[0] : t).pageX - o,
                        l = (e ? t.originalEvent.changedTouches[0] : t).pageY - n;
                    (i || v) && s === p && l === h && x.setHand(s, l), "hours" === x.currentView ? x.toggleView("minutes", A / 2) : r.autoclose && (x.minutesView.addClass("clockpicker-dial-out"), setTimeout(function() {
                        x.done()
                    }, A / 2)), u.prepend(j), clearTimeout(m), c.removeClass("clockpicker-moving"), a.off(d)
                })
            }
        }
        var h = n(V),
            u = h.find(".clockpicker-plate"),
            v = h.find(".clockpicker-hours"),
            m = h.find(".clockpicker-minutes"),
            T = h.find(".clockpicker-am-pm-block"),
            C = "INPUT" === s.prop("tagName"),
            H = C ? s : s.find("input"),
            P = s.find(".input-group-addon"),
            x = this;
        if (this.id = e("cp"), this.element = s, this.options = r, this.isAppended = !1, this.isShown = !1, this.currentView = "hours", this.isInput = C, this.input = H, this.addon = P, this.popover = h, this.plate = u, this.hoursView = v, this.minutesView = m, this.amPmBlock = T, this.spanHours = h.find(".clockpicker-span-hours"), this.spanMinutes = h.find(".clockpicker-span-minutes"), this.spanAmPm = h.find(".clockpicker-span-am-pm"), this.amOrPm = "PM", r.twelvehour) {
            {
                var S = ['<div class="clockpicker-am-pm-block">', '<button type="button" class="btn btn-sm btn-default clockpicker-button clockpicker-am-button">', "AM</button>", '<button type="button" class="btn btn-sm btn-default clockpicker-button clockpicker-pm-button">', "PM</button>", "</div>"].join("");
                n(S)
            }
            n('<button type="button" class="btn btn-sm btn-default clockpicker-button am-button">AM</button>').on("click", function() {
                x.amOrPm = "AM", n(".clockpicker-span-am-pm").empty().append("AM")
            }).appendTo(this.amPmBlock), n('<button type="button" class="btn btn-sm btn-default clockpicker-button pm-button">PM</button>').on("click", function() {
                x.amOrPm = "PM", n(".clockpicker-span-am-pm").empty().append("PM")
            }).appendTo(this.amPmBlock)
        }
        r.autoclose || n('<button type="button" class="btn btn-sm btn-default btn-block clockpicker-button">' + r.donetext + "</button>").click(n.proxy(this.done, this)).appendTo(h), "top" !== r.placement && "bottom" !== r.placement || "top" !== r.align && "bottom" !== r.align || (r.align = "left"), "left" !== r.placement && "right" !== r.placement || "left" !== r.align && "right" !== r.align || (r.align = "top"), h.addClass(r.placement), h.addClass("clockpicker-align-" + r.align), this.spanHours.click(n.proxy(this.toggleView, this, "hours")), this.spanMinutes.click(n.proxy(this.toggleView, this, "minutes")), H.on("focus.clockpicker click.clockpicker", n.proxy(this.show, this)), P.on("click.clockpicker", n.proxy(this.toggle, this));
        var E, D, I, B, z = n('<div class="clockpicker-tick"></div>');
        if (r.twelvehour)
            for (E = 1; 13 > E; E += 1) D = z.clone(), I = E / 6 * Math.PI, B = g, D.css("font-size", "120%"), D.css({
                left: b + Math.sin(I) * B - y,
                top: b - Math.cos(I) * B - y
            }), D.html(0 === E ? "00" : E), v.append(D), D.on(k, p);
        else
            for (E = 0; 24 > E; E += 1) {
                D = z.clone(), I = E / 6 * Math.PI;
                var O = E > 0 && 13 > E;
                B = O ? w : g, D.css({
                    left: b + Math.sin(I) * B - y,
                    top: b - Math.cos(I) * B - y
                }), O && D.css("font-size", "120%"), D.html(0 === E ? "00" : E), v.append(D), D.on(k, p)
            }
        for (E = 0; 60 > E; E += 5) D = z.clone(), I = E / 30 * Math.PI, D.css({
            left: b + Math.sin(I) * g - y,
            top: b - Math.cos(I) * g - y
        }), D.css("font-size", "120%"), D.html(i(E)), m.append(D), D.on(k, p);
        if (u.on(k, function(t) {
                0 === n(t.target).closest(".clockpicker-tick").length && p(t, !0)
            }), l) {
            var j = h.find(".clockpicker-canvas"),
                L = t("svg");
            L.setAttribute("class", "clockpicker-svg"), L.setAttribute("width", M), L.setAttribute("height", M);
            var U = t("g");
            U.setAttribute("transform", "translate(" + b + "," + b + ")");
            var W = t("circle");
            W.setAttribute("class", "clockpicker-canvas-bearing"), W.setAttribute("cx", 0), W.setAttribute("cy", 0), W.setAttribute("r", 2);
            var N = t("line");
            N.setAttribute("x1", 0), N.setAttribute("y1", 0);
            var X = t("circle");
            X.setAttribute("class", "clockpicker-canvas-bg"), X.setAttribute("r", y);
            var Y = t("circle");
            Y.setAttribute("class", "clockpicker-canvas-fg"), Y.setAttribute("r", 3.5), U.appendChild(N), U.appendChild(X), U.appendChild(Y), U.appendChild(W), L.appendChild(U), j.append(L), this.hand = N, this.bg = X, this.fg = Y, this.bearing = W, this.g = U, this.canvas = j
        }
        o(this.options.init)
    }

    function o(t) {
        t && "function" == typeof t && t()
    }
    var c, n = window.jQuery,
        r = n(window),
        a = n(document),
        p = "http://www.w3.org/2000/svg",
        l = "SVGAngle" in window && function() {
            var t, i = document.createElement("div");
            return i.innerHTML = "<svg/>", t = (i.firstChild && i.firstChild.namespaceURI) == p, i.innerHTML = "", t
        }(),
        h = function() {
            var t = document.createElement("div").style;
            return "transition" in t || "WebkitTransition" in t || "MozTransition" in t || "msTransition" in t || "OTransition" in t
        }(),
        u = "ontouchstart" in window,
        k = "mousedown" + (u ? " touchstart" : ""),
        d = "mousemove.clockpicker" + (u ? " touchmove.clockpicker" : ""),
        f = "mouseup.clockpicker" + (u ? " touchend.clockpicker" : ""),
        v = navigator.vibrate ? "vibrate" : navigator.webkitVibrate ? "webkitVibrate" : null,
        m = 0,
        b = 100,
        g = 80,
        w = 54,
        y = 13,
        M = 2 * b,
        A = h ? 350 : 1,
        V = ['<div class="popover clockpicker-popover">', '<div class="arrow"></div>', '<div class="popover-title">', '<span class="clockpicker-span-hours text-primary"></span>', " : ", '<span class="clockpicker-span-minutes"></span>', '<span class="clockpicker-span-am-pm"></span>', "</div>", '<div class="popover-content">', '<div class="clockpicker-plate">', '<div class="clockpicker-canvas"></div>', '<div class="clockpicker-dial clockpicker-hours"></div>', '<div class="clockpicker-dial clockpicker-minutes clockpicker-dial-out"></div>', "</div>", '<span class="clockpicker-am-pm-block">', "</span>", "</div>", "</div>"].join("");
    s.DEFAULTS = {
        "default": "",
        fromnow: 0,
        placement: "bottom",
        align: "left",
        donetext: "完成",
        autoclose: !1,
        twelvehour: !1,
        vibrate: !0
    }, s.prototype.toggle = function() {
        this[this.isShown ? "hide" : "show"]()
    }, s.prototype.locate = function() {
        var t = this.element,
            i = this.popover,
            e = t.offset(),
            s = t.outerWidth(),
            o = t.outerHeight(),
            c = this.options.placement,
            n = this.options.align,
            r = {};
        switch (i.show(), c) {
            case "bottom":
                r.top = e.top + o;
                break;
            case "right":
                r.left = e.left + s;
                break;
            case "top":
                r.top = e.top - i.outerHeight();
                break;
            case "left":
                r.left = e.left - i.outerWidth()
        }
        switch (n) {
            case "left":
                r.left = e.left;
                break;
            case "right":
                r.left = e.left + s - i.outerWidth();
                break;
            case "top":
                r.top = e.top;
                break;
            case "bottom":
                r.top = e.top + o - i.outerHeight()
        }
        i.css(r)
    }, s.prototype.show = function() {
        if (!this.isShown) {
            o(this.options.beforeShow);
            var t = this;
            this.isAppended || (c = n(document.body).append(this.popover), r.on("resize.clockpicker" + this.id, function() {
                t.isShown && t.locate()
            }), this.isAppended = !0);
            var e = ((this.input.prop("value") || this.options["default"] || "") + "").split(":");
            if ("now" === e[0]) {
                var s = new Date(+new Date + this.options.fromnow);
                e = [s.getHours(), s.getMinutes()]
            }
            this.hours = +e[0] || 0, this.minutes = +e[1] || 0, this.spanHours.html(i(this.hours)), this.spanMinutes.html(i(this.minutes)), this.toggleView("hours"), this.locate(), this.isShown = !0, a.on("click.clockpicker." + this.id + " focusin.clockpicker." + this.id, function(i) {
                var e = n(i.target);
                0 === e.closest(t.popover).length && 0 === e.closest(t.addon).length && 0 === e.closest(t.input).length && t.hide()
            }), a.on("keyup.clockpicker." + this.id, function(i) {
                27 === i.keyCode && t.hide()
            }), o(this.options.afterShow)
        }
    }, s.prototype.hide = function() {
        o(this.options.beforeHide), this.isShown = !1, a.off("click.clockpicker." + this.id + " focusin.clockpicker." + this.id), a.off("keyup.clockpicker." + this.id), this.popover.hide(), o(this.options.afterHide)
    }, s.prototype.toggleView = function(t, i) {
        var e = !1;
        "minutes" === t && "visible" === n(this.hoursView).css("visibility") && (o(this.options.beforeHourSelect), e = !0);
        var s = "hours" === t,
            c = s ? this.hoursView : this.minutesView,
            r = s ? this.minutesView : this.hoursView;
        this.currentView = t, this.spanHours.toggleClass("text-primary", s), this.spanMinutes.toggleClass("text-primary", !s), r.addClass("clockpicker-dial-out"), c.css("visibility", "visible").removeClass("clockpicker-dial-out"), this.resetClock(i), clearTimeout(this.toggleViewTimer), this.toggleViewTimer = setTimeout(function() {
            r.css("visibility", "hidden")
        }, A), e && o(this.options.afterHourSelect)
    }, s.prototype.resetClock = function(t) {
        var i = this.currentView,
            e = this[i],
            s = "hours" === i,
            o = Math.PI / (s ? 6 : 30),
            c = e * o,
            n = s && e > 0 && 13 > e ? w : g,
            r = Math.sin(c) * n,
            a = -Math.cos(c) * n,
            p = this;
        l && t ? (p.canvas.addClass("clockpicker-canvas-out"), setTimeout(function() {
            p.canvas.removeClass("clockpicker-canvas-out"), p.setHand(r, a)
        }, t)) : this.setHand(r, a)
    }, s.prototype.setHand = function(t, e, s, o) {
        var c, r = Math.atan2(t, -e),
            a = "hours" === this.currentView,
            p = Math.PI / (a || s ? 6 : 30),
            h = Math.sqrt(t * t + e * e),
            u = this.options,
            k = a && (g + w) / 2 > h,
            d = k ? w : g;
        if (u.twelvehour && (d = g), 0 > r && (r = 2 * Math.PI + r), c = Math.round(r / p), r = c * p, u.twelvehour ? a ? 0 === c && (c = 12) : (s && (c *= 5), 60 === c && (c = 0)) : a ? (12 === c && (c = 0), c = k ? 0 === c ? 12 : c : 0 === c ? 0 : c + 12) : (s && (c *= 5), 60 === c && (c = 0)), this[this.currentView] !== c && v && this.options.vibrate && (this.vibrateTimer || (navigator[v](10), this.vibrateTimer = setTimeout(n.proxy(function() {
                this.vibrateTimer = null
            }, this), 100))), this[this.currentView] = c, this[a ? "spanHours" : "spanMinutes"].html(i(c)), !l) return void this[a ? "hoursView" : "minutesView"].find(".clockpicker-tick").each(function() {
            var t = n(this);
            t.toggleClass("active", c === +t.html())
        });
        o || !a && c % 5 ? (this.g.insertBefore(this.hand, this.bearing), this.g.insertBefore(this.bg, this.fg), this.bg.setAttribute("class", "clockpicker-canvas-bg clockpicker-canvas-bg-trans")) : (this.g.insertBefore(this.hand, this.bg), this.g.insertBefore(this.fg, this.bg), this.bg.setAttribute("class", "clockpicker-canvas-bg"));
        var f = Math.sin(r) * d,
            m = -Math.cos(r) * d;
        this.hand.setAttribute("x2", f), this.hand.setAttribute("y2", m), this.bg.setAttribute("cx", f), this.bg.setAttribute("cy", m), this.fg.setAttribute("cx", f), this.fg.setAttribute("cy", m)
    }, s.prototype.done = function() {
        o(this.options.beforeDone), this.hide();
        var t = this.input.prop("value"),
            e = i(this.hours) + ":" + i(this.minutes);
        this.options.twelvehour && (e += this.amOrPm), this.input.prop("value", e), e !== t && (this.input.triggerHandler("change"), this.isInput || this.element.trigger("change")), this.options.autoclose && this.input.trigger("blur"), o(this.options.afterDone)
    }, s.prototype.remove = function() {
        this.element.removeData("clockpicker"), this.input.off("focus.clockpicker click.clockpicker"), this.addon.off("click.clockpicker"), this.isShown && this.hide(), this.isAppended && (r.off("resize.clockpicker" + this.id), this.popover.remove())
    }, n.fn.clockpicker = function(t) {
        var i = Array.prototype.slice.call(arguments, 1);
        return this.each(function() {
            var e = n(this),
                o = e.data("clockpicker");
            if (o) "function" == typeof o[t] && o[t].apply(o, i);
            else {
                var c = n.extend({}, s.DEFAULTS, e.data(), "object" == typeof t && t);
                e.data("clockpicker", new s(e, c))
            }
        })
    }
}();





//TinyMCE
tinymce.init({
    selector: '.tinymce',
    height: 500,
    skin_url: '/css/tinymce/lightgray',
    plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
    ],
    toolbar1: 'print | undo redo | insert | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | help',
    image_advtab: true,
    templates: [
        { title: 'Event Table', content: '<table style="width: 100%; margin-left: auto; margin-right: auto;" cellpadding="0pt 5.4pt">' +
        '<tbody>' +
        '<tr style="height: 18.8pt;">' +
            '<td style="width: 95.625px; border: 1pt solid windowtext; background: #00b0f0; padding: 0pt 5.4pt; height: 18.8pt;"><strong>TIME</strong></td>' +
            '<td style="width: 358.625px; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: #00b0f0; padding: 0pt 5.4pt; height: 18.8pt;"><strong>DESCRIPTION</strong></td>' +
            '<td style="width: 130.625px; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none; background: #00b0f0; padding: 0pt 5.4pt; height: 18.8pt;"><strong>LOCATION</strong></td>' +
        '</tr>' +
        '<tr style="height: 15.75pt;">' +
            '<td style="width: 95.625px; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-left: 1pt solid windowtext; border-image: initial; border-top: none; padding: 0pt 5.4pt; height: 15.75pt;">&nbsp;</td>' +
            '<td style="width: 358.625px; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; padding: 0pt 5.4pt; height: 15.75pt;">&nbsp;</td>' +
            '<td style="width: 130.625px; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; padding: 0pt 5.4pt; height: 15.75pt;">&nbsp;</td>' +
        '</tr>' +
        '<tr style="height: 15.75pt;">' +
            '<td style="width: 95.625px; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-left: 1pt solid windowtext; border-image: initial; border-top: none; padding: 0pt 5.4pt; height: 15.75pt;">&nbsp;</td>' +
            '<td style="width: 358.625px; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; padding: 0pt 5.4pt; height: 15.75pt;">&nbsp;</td>' +
            '<td style="width: 130.625px; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; padding: 0pt 5.4pt; height: 15.75pt;">&nbsp;</td>' +
        '</tr>' +
        '<tr style="height: 15.75pt;">' +
            '<td style="width: 95.625px; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-left: 1pt solid windowtext; border-image: initial; border-top: none; padding: 0pt 5.4pt; height: 15.75pt;">&nbsp;</td>' +
            '<td style="width: 358.625px; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; padding: 0pt 5.4pt; height: 15.75pt;">&nbsp;</td>' +
            '<td style="width: 130.625px; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext; padding: 0pt 5.4pt; height: 15.75pt;">&nbsp;</td>' +
        '</tr>' },
    ],
    table_appearance_options: false,
    table_default_styles: {
        width: '100%'
    },
    paste_retain_style_properties:'all',
    paste_word_valid_elements: "table,tr,td,b,strong,i,em,h1,h2",
    paste_data_images: true,
});

//daterange pickers
$(document).ready(function() {
    var minDate = moment();

    $('.single-daterange-with-time-with-past').daterangepicker({
        "singleDatePicker": true,
        "timePicker": true,
        "timePicker24Hour": true,
        "timePickerIncrement": 15,
        "locale": {
            "format": "DD/MM/YYYY H:mm",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
        },
        "opens": "center",
    });

    $('.single-daterange-with-time').daterangepicker({
        "minDate": minDate,
        "singleDatePicker": true,
        "timePicker": true,
        "timePicker24Hour": true,
        "timePickerIncrement": 15,
        "locale": {
            "format": "DD/MM/YYYY H:mm",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
        },
        "opens": "center",
    });

    $('.single-daterange').daterangepicker({
        "minDate": minDate,
        "singleDatePicker": true,
        "timePicker": false,
        "timePicker24Hour": false,
        "timePickerIncrement": 15,
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
        },
        "opens": "center",
    });

    $('.single-date').daterangepicker({
        "singleDatePicker": true,
        "timePicker": false,
        "timePicker24Hour": false,
        "timePickerIncrement": 15,
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
        },
        "opens": "center",
    });

    $('.single-time').daterangepicker({
        "singleDatePicker": true,
        "timePicker": true,
        "timePicker24Hour": true,
        "timePickerIncrement": 15,
        "locale": {
            "format": "HH:mm",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
        },
        "opens": "center",
    });
});

//iCheck
$(document).ready(function() {
    $('.icheck').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });
});

// image picker with bootstrap thumbnails
$(document).ready(function() {
    if($('.grid').length > 0) {
        var iso = new Isotope('.grid', {
            layoutMode: 'masonry'
        });
        $('.modal').on('shown.bs.modal', function (e) {
            iso.layout();
        });
    }
});

// Dropzone
Dropzone.autoDiscover = false;
$(document).ready(function() {
    if($('.dropzone').length > 0) {
        var dropzone = new Dropzone(".dropzone");
        dropzone.on("success", function (file) {
            $('#next.btn').prop('disabled', false);
        });
    }
});
// Dropzone

/**
 * Resize function without multiple trigger
 *
 * Usage:
 * $(window).smartresize(function(){
 *     // code here
 * });
 */
(function($,sr){
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
        var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            }

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    };

    // smartresize
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');