@extends('layouts.blank', [])

@push('stylesheets')
@endpush

@section('main_container')
    <div id="innerWrap">
        <div id="pageInner">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                {{ucfirst($pokemon->name)}}
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <tbody>
                                        <tr>
                                            <td>Base Experiance</td>
                                            <td colspan="8">{{$pokemon->base_experience}}</td>
                                        </tr>
                                        <tr>
                                            <td>Species</td>
                                            <td colspan="8">{{ucfirst($pokemon->species)}}</td>
                                        </tr>
                                        <tr>
                                            <td>Height</td>
                                            <td colspan="8">{{$pokemon->height}}</td>
                                        </tr>
                                        <tr>
                                            <td>Weight</td>
                                            <td colspan="8">{{$pokemon->weight}}</td>
                                        </tr>
                                        <tr>
                                            <td>Sprites</td>
                                            <td>
                                                <strong>Front</strong><br/>
                                                @if(!empty($pokemon->sprites) && isset($pokemon->sprites->front_default))<img src="{{$pokemon->sprites->front_default}}" />@else Image not found @endif
                                            </td>
                                            <td>
                                                <strong>Front Shiny</strong><br/>
                                                @if(!empty($pokemon->sprites) && isset($pokemon->sprites->front_shiny))<img src="{{$pokemon->sprites->front_shiny}}" />@else Image not found @endif
                                            </td>
                                            <td>
                                                <strong>Front Female</strong><br/>
                                                @if(!empty($pokemon->sprites) && isset($pokemon->sprites->front_female))<img src="{{$pokemon->sprites->front_female}}" />@else Image not found @endif
                                            </td>
                                            <td>
                                                <strong>Front Female Shiny</strong><br/>
                                                @if(!empty($pokemon->sprites) && isset($pokemon->sprites->front_shiny_female))<img src="{{$pokemon->sprites->front_shiny_female}}" />@else Image not found @endif
                                            </td>
                                            <td>
                                                <strong>Back</strong><br/>
                                                @if(!empty($pokemon->sprites) && isset($pokemon->sprites->back_default))<img src="{{$pokemon->sprites->back_default}}" />@else Image not found @endif
                                            </td>
                                            <td>
                                                <strong>Back Shiney</strong><br/>
                                                @if(!empty($pokemon->sprites) && isset($pokemon->sprites->back_shiny))<img src="{{$pokemon->sprites->back_shiny}}" />@else Image not found @endif
                                            </td>
                                            <td>
                                                <strong>Back Female</strong><br/>
                                                @if(!empty($pokemon->sprites) && isset($pokemon->sprites->back_female))<img src="{{$pokemon->sprites->back_female}}" />@else Image not found @endif
                                            </td>
                                            <td>
                                                <strong>Back Female Shiney</strong><br/>
                                                @if(!empty($pokemon->sprites) && isset($pokemon->sprites->back_shiny_female))<img src="{{$pokemon->sprites->back_shiny_female}}" />@else Image not found @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Abilities</td>
                                            <td colspan="8">
                                                <table class="table table-striped jambo_table bulk_action">
                                                    <thead>
                                                        <tr class="headings">
                                                            <th class="column-title">Name</th>
                                                            <th class="column-title">Slot</th>
                                                            <th class="column-title">Is Hidden</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($pokemon->abilities as $ability)
                                                        <tr>
                                                            <td>{{ucfirst($ability->name)}}</td>
                                                            <td>{{$ability->slot}}</td>
                                                            <td>@if($ability->is_hidden) True @else False @endif</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                    <a href="{{route('pokemon.index')}}" class="btn btn-primary" data-toggle="tooltip" data-placement="left" data-original-title="Back to All Pokemon.">Back to All Pokemon</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@endpush