<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <title>{{ config('app.name') }}</title>

        <!-- App Style -->
        {{ Html::style(mix('css/app.css')) }}
        <!-- Blade Styles -->
        @stack('stylesheets')

    </head>

    <body class="nav-sm">
        <div id="app" class="container body">
            <div class="main_container">

                @include('includes/sidebar')

                @include('includes/topbar')

                <div class="right_col" role="main">
                    @if( !empty($main_title) )
                        <div class="page-title clearfix">
                            <div class="title_left">
                                <h3>{{ $main_title }}</h3>
                            </div>

                            @if( !empty($search_route) )
                                <div class="title_right">
                                    {!! Form::searchfrm( $search_route, 'col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search', !empty($search_append) ? $search_append : null ) !!}
                                </div>
                            @endif
                        </div>
                    @endif

                    @include('includes/flash')
                        <div id="parsley_error_div" class="alert alert-danger alert-dismissable fade in" style="display: none">
                            <a href="#" class="close" id="parsley_error_close" >&times;</a>
                            <span id="parsley_error_msg"> There was an error with your form submission, please review below.</span>
                        </div>

                    @yield('main_container')

                </div>

                @include('includes/footer')
            </div>
        </div>

        <!-- App Scripts -->
        {{ Html::script(mix('js/app.js')) }}

        <!-- Blade Scripts -->
        @stack('scripts')
        <script>
            $('#parsley_error_close').on('click', function () {
                $('#parsley_error_div').hide();
            });
            window.Parsley.on('field:error', function() {
                // This global callback will be called for any field that fails validation.
                $('#parsley_error_div').show();
            });
        </script>
    </body>
</html>