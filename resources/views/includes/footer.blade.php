<!-- footer content -->
<footer>
    <div class="pull-right">
        {{ config('app.name') }} &copy; {{date('Y')}}
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->