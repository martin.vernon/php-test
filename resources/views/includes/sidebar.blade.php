<div class="col-md-3 left_col">
    <div class="left_col scroll-view">        
        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="{{ Gravatar::src('martin.vernon@gmail.com') }}" alt="Avatar of Martin Vernon" class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>Martin Vernon</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->
        
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a>
                            {{ HTML::image('images/small-logo.png', config('app.name'), ['height'=>'37px', 'width'=>'37px']) }}
                            <span class="menu-label">Pokedex</span><span class="fas fa-chevron-down "></span>
                        </a>
                        <ul class="nav child_menu" style="display:none;">
                            <li>
                                <a href="{{route('pokemon.index')}}"> All Pokemon</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>