@if( !empty( request()->session()->exists('success') ) )
    <div class="alert alert-success alert-dismissable fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ request()->session()->get('success') }}
    </div>
@endif
@if( !empty( request()->session()->exists('message') ) )
    <div class="alert alert-info alert-dismissable fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ request()->session()->get('message') }}
    </div>
@endif
@if( !empty( request()->session()->exists('warning') ) )
    <div class="alert alert-warning alert-dismissable fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ request()->session()->get('warning') }}
    </div>
@endif
@if( !empty( request()->session()->exists('error') ) )
    <div class="alert alert-danger alert-dismissable fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ request()->session()->get('error') }}
    </div>
@endif


