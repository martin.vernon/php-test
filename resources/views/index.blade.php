@extends('layouts.blank', [
    'main_title' => 'Pokedex',
    'search_route' => 'pokemon.index'
])

@push('stylesheets')
@endpush

@section('main_container')
    <div id="innerWrap">
        <div id="pageInner">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                All Pokemon
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($pokemon) && !empty($pokemon) && sizeof($pokemon) > 0 )
                                            @foreach($pokemon as $poke)
                                                <tr>
                                                    <td>
                                                        <a href="{{route('pokemon.show', $poke)}}" title="{{$poke->name}}">
                                                            @if(!empty($poke->sprites))<img src="{{$poke->sprites->front_default}}" />@endif
                                                            {{ucfirst($poke->name)}}
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @elseif( !empty($s) )
                                            <tr>
                                                <td class="text-center empty">There are no Pokemon found with your search term <strong><i>{{$s}}</i></strong>. Please search again.</td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td class="text-center empty">There are no Pokemon available</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                                {{ $pokemon->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@endpush